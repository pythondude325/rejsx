const flat=(a)=>[].concat(...a.map(A=>A instanceof Array?flat(A):A))

window.React = {
  createElement(n,a,...c){
    let e=typeof n=="function"?n():document.createElement(n)
    if(a)for(let A in a)e.setAttribute(A,a[A])
    flat(c).forEach(C=>e.appendChild(C instanceof Node?C:new Text(C)))
    return e
  },
  Fragment:()=>document.createDocumentFragment()
}
